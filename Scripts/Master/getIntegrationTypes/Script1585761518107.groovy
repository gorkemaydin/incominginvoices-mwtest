import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

import base.CommonBaseModel
import base.ResponseBaseModal
import master.IntegratorTypeListItemModal

import util.JsonUtil as JSON

def resp = WS.sendRequest(findTestObject('Master/getIntegrationType', [('BaseURL') : GlobalVariable.BaseURL, ('Language') : GlobalVariable.Language
            , ('DeviceType') : GlobalVariable.DeviceType, ('DeviceId') : GlobalVariable.DeviceId]))

assert resp.statusCode == 200

def jsonUtil = new JSON()
ResponseBaseModal<CommonBaseModel<IntegratorTypeListItemModal[]>> base = jsonUtil.parse(resp.getResponseText())

assert base.getCode() == 200 
assert base.getIsError() == false




